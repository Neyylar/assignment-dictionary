%define last_pointer 0
%macro colon 2
	%2:
	dq last_pointer
	db %1, 0
	%define last_pointer %2
%endmacro

