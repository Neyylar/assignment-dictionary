
section .text
 
%define SYS_READ 0 
%define SYS_WRITE 1 
%define SYS_EXIT 60
%define ASCII_CONSTANT 0x30
%define STDOUT 1

; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, SYS_EXIT
	syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
 	call string_length
 	mov rdx, rax
	mov rax, SYS_WRITE
	mov rdi, r9
	syscall
	pop rdi
	ret
	
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, '\n'
	
; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, SYS_WRITE 
	mov rdx, 1 
	mov rdi, STDOUT
	mov rsi, rsp
	syscall
	pop rdi
	ret




; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	mov r9, rsp
	mov rax, rdi
	mov r10, 10
	dec rsp
	.loop:
		xor rdx, rdx
		div r10
		add rdx, ASCII_CONSTANT	
		dec rsp
		mov byte [rsp], dl
		cmp rax, 0
		jnz .loop

		mov rdi, rsp
		push r9
		mov r9, 1
		push rsi
		call print_string
		pop rsi
		pop r9
		mov rsp, r9
		ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint
	neg rdi
	mov r8, rdi
	mov rdi, '-'
	jmp print_char
	mov rdi, r8
	jmp print_uint
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov al, byte [rdi]
	cmp al, byte [rsi]
	jne .not_equal
	inc rdi
	inc rsi
	test al, al
	jnz string_equals
	mov rax, 1
	ret
	.not_equal:
		mov rax, 0
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	mov rax, SYS_READ
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	dec r13
	xor r14, r14
	.loop:
		call read_char
		cmp r13, r14
		je .error
		cmp rax, 0x0
		je .success
		cmp rax, 0x20
		je .check
		cmp rax, 0x9
		je .check
		cmp rax, 0xA
		je .check
		mov byte[r12+r14], al
		inc r14
		jmp .loop
	.check:
		test r14, r14
		je .loop
	.success:
		mov byte[r12+r14], 0
		mov rax, r12
		mov rdx, r14
		jmp .end
	.error:
		xor rax, rax
	.end: 
		pop r14
		pop r13
		pop r12
		ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor r8, r8
	xor r9, r9
	mov r10, 10
	.loop:
		mov r9b, [rdi+r8]
		cmp r9b, 0x30
		jb .end
		cmp r9b, 0x39
		ja .end
		sub r9b, 0x30
		mul r10
		add rax, r9
		inc r8
		jmp .loop
	.end:
		mov rdx, r8
		ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	cmp byte [rdi], '-'
	je .is_negative
	jmp parse_uint
	.is_negative:
		inc rdi
		call parse_uint
		neg rax
		test rdx, rdx
		jz .end
		inc rdx
	.end:
		ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jge .overflow
	xor rcx, rcx 
	.loop:
		mov r9b, byte[rdi+rcx]
		mov byte[rsi+rcx], r9b
		cmp rcx, rax
		je .end
		inc rcx
		jmp .loop
	.overflow:
		xor rax, rax
		ret
	.end:
	ret
