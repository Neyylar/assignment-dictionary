%include "colon.inc"
%include "words.inc"
%include "lib.asm"

%define buffer_size 256
%define STDOUT 1
%define STDERR 2
%define FALSE_CODE 1
%define buffer_size 256

extern find_word
global _start

section .rodata
	error_key: db "There is no such key in the dictionary", 0
	error_buffer_size: db "Key is too long", 0
section .data
	buffer: times buffer_size db 0

section .text

_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	test rax, rax
	je .overflow_error
	mov rsi, last_pointer
	mov rdi, rax
	call find_word
	test rax, rax
	je .key_error
	add rax, rdx
	add rax, 8
	add rax, 1 
	mov rdi, rax
	push r9
	mov r9, STDOUT
	call print_string
	pop r9
.end:
	call print_newline
	call exit

.overflow_error:
	mov rdi, error_buffer_size
	jmp .print_error

.key_error:
	mov rdi, error_key
.print_error:
	push r9
	mov r9, STDERR
	call print_string
	pop r9
	call print_newline
	mov rax, SYS_EXIT
	mov rdi, FALSE_CODE
	syscall

	


