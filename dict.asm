%include "lib.inc"
global find_word

find_word:
	xor rax, rax 
	.loop:
		push rsi
		add rsi, 8 
		call string_equals
		pop rsi 
		cmp rax, 1 
		je .end 
		mov rsi, [rsi]
		test rsi, rsi
		je .error
		jmp .loop
		
	.error:
		xor rax, rax
		ret
		
	.end:
		mov rax, rsi
		ret

